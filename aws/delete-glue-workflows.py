import boto3
import re

def delete_glue_workflows(include_patterns, exclude_patterns=None, regions=None):
    """
    Deletes AWS Glue workflows across specified regions based on include patterns and excluding any that match patterns in exclude_patterns,
    after user confirmation, with pagination handled.

    :param include_patterns: List of string patterns to match workflow names against for inclusion.
    :param exclude_patterns: List of string patterns to exclude workflows from deletion.
    :param regions: List of AWS regions to operate in. If None, uses the default region.
    """
    if exclude_patterns is None:
        exclude_patterns = []
    if regions is None:
        regions = ['us-east-1']

    for region in regions:
        print(f"\nProcessing region: {region}")
        glue_client = boto3.client('glue', region_name=region)

        try:
            matching_workflows = []
            next_token = None

            # Handle pagination
            while True:
                if next_token:
                    response = glue_client.list_workflows(NextToken=next_token)
                else:
                    response = glue_client.list_workflows()

                if 'Workflows' in response:
                    for workflow in response['Workflows']:
                        # Check if the workflow matches any include pattern and does not match any exclude pattern
                        if any(re.search(pattern, workflow) for pattern in include_patterns) and not any(re.search(pattern, workflow) for pattern in exclude_patterns):
                            matching_workflows.append(workflow)

                next_token = response.get('NextToken')
                if not next_token:
                    break

            if not matching_workflows:
                print("No matching workflows found in region:", region)
                continue

            print(f"The following workflows in region {region} match the include patterns and do not match any exclude patterns. They will be considered for deletion:")
            for workflow_name in matching_workflows:
                print(workflow_name)

            user_input = input("Do you want to delete these workflows? (yes/no): ")
            if user_input.lower() == 'yes':
                for workflow_name in matching_workflows:
                    print(f"Deleting workflow: {workflow_name}")
                    delete_response = glue_client.delete_workflow(Name=workflow_name)
                    print(f"Deleted workflow: {workflow_name}, Status: {delete_response['ResponseMetadata']['HTTPStatusCode']}")
            else:
                print("Deletion cancelled by user.")
        except Exception as e:
            print(f"An error occurred in region {region}: {str(e)}")

def main():

    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-qa-h-.*gluelog.*", "ccapi-customer-api-qa-r-.*gluelog.*"]   # Regex patterns for glue workflows to delete
    # exclude_patterns = ["qa-release", "cap-release", "r-next"]                                          # Regex patterns for glue workflows to exclude from deletion
    # regions = ['us-east-1' 'us-east-2']


    include_patterns = []  
    exclude_patterns = []
    regions = ['us-east-1', 'us-east-2']
    delete_glue_workflows(include_patterns, exclude_patterns, regions)

if __name__ == '__main__':
    main()

