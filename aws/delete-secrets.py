import boto3
import re

def remove_replicas_and_delete_secret(secret_name, primary_region):
    """
    Removes replicas of a secret and deletes the primary secret.
    :param secret_name: The name of the secret to delete.
    :param primary_region: The AWS region where the primary secret resides.
    """
    primary_client = boto3.client('secretsmanager', region_name=primary_region)
    try:
        secret_info = primary_client.describe_secret(SecretId=secret_name)
        if 'ReplicationStatus' in secret_info:
            # Get the regions where the secret is replicated
            replica_regions = [replica['Region'] for replica in secret_info['ReplicationStatus']]
            if replica_regions:
                # Remove the replicas first to be able to delete the primary secret
                primary_client.remove_regions_from_replication(SecretId=secret_name, RemoveReplicaRegions=replica_regions)
                print(f"Removed replicas of secret {secret_name} from regions: {', '.join(replica_regions)}.")
        primary_client.delete_secret(SecretId=secret_name, ForceDeleteWithoutRecovery=True)
        print(f"Deleted primary secret {secret_name} from region {primary_region}.\n")
    except Exception as e:
        print(f"Failed to remove replicas and delete secret {secret_name}: {e}")

def delete_secrets_immediately(include_patterns, exclude_patterns=None, regions=None):
    """
    Deletes secrets immediately across specified regions based on include and exclude patterns.
    """
    if exclude_patterns is None:
        exclude_patterns = []
    if regions is None:
        regions = ['us-east-1']

    for region in regions:
        print(f"\nProcessing region: {region}")
        client = boto3.client('secretsmanager', region_name=region)

        next_token = None
        secrets_to_delete = []

        while True:
            if next_token:
                response = client.list_secrets(MaxResults=20, NextToken=next_token)
            else:
                response = client.list_secrets(MaxResults=20)

            for secret in response.get('SecretList', []):
                secret_name = secret['Name']
                if any(re.search(pattern, secret_name) for pattern in include_patterns) and not any(re.search(pattern, secret_name) for pattern in exclude_patterns):
                    secrets_to_delete.append(secret_name)

            next_token = response.get('NextToken', None)
            if not next_token:
                break

        if not secrets_to_delete:
            print(f"No matching secrets found for deletion criteria in region {region}.")
            continue

        print(f"Secrets matching criteria in region {region}:")
        for secret_name in secrets_to_delete:
            print(secret_name)

        user_input = input("Do you want to delete these secrets? (yes/no): ")
        if user_input.lower() == 'yes':
            for secret_name in secrets_to_delete:
                # Describe the secret to find out the primary region
                secret_info = client.describe_secret(SecretId=secret_name)
                primary_region = secret_info.get('PrimaryRegion', region)
                remove_replicas_and_delete_secret(secret_name, primary_region)
        else:
            print("Deletion cancelled by user.")

def main():
    
    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*glue.*"]             # Regex patterns for secrets to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]         # Regex patterns for secrets to exclude from deletion
    # regions = ['us-east-1' 'us-east-2']


    include_patterns = []
    exclude_patterns = []
    # At this time our secrets are only replicated from us-east-1 to us-east-2
    # so just passing in the primary region will take care of both region's secrets 
    # since this script will delete the replica before the primary 
    regions = ['us-east-1'] 
    delete_secrets_immediately(include_patterns, exclude_patterns, regions)

if __name__ == '__main__':
    main()

