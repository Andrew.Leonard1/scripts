import boto3
import re

def delete_lambda_functions(include_patterns, exclude_patterns=None, regions=None):
    """
    Deletes AWS Lambda functions across specified regions based on include and exclude regex patterns,
    after user confirmation

    :param include_patterns: List of regex patterns to match Lambda function names against for inclusion.
    :param exclude_patterns: List of regex patterns to exclude Lambda functions from deletion.
    :param regions: List of AWS regions to operate in. If no regions are passed in, use the default region.
    """
    if exclude_patterns is None:
        exclude_patterns = []
    if regions is None:
        regions = ['us-east-1']

    for region in regions:
        print(f"\nProcessing region: {region}")
        lambda_client = boto3.client('lambda', region_name=region)

        try:
            matching_functions = []
            next_marker = None

            # Handle pagination
            while True:
                if next_marker:
                    response = lambda_client.list_functions(Marker=next_marker)
                else:
                    response = lambda_client.list_functions()

                if 'Functions' in response:
                    for function in response['Functions']:
                        function_name = function['FunctionName']
                        # Check if the function name matches any of the include patterns and does not match any of the exclude patterns
                        if any(re.search(pattern, function_name) for pattern in include_patterns) and not any(re.search(pattern, function_name) for pattern in exclude_patterns):
                            matching_functions.append(function_name)

                next_marker = response.get('NextMarker')
                if not next_marker:
                    break

            if not matching_functions:
                print("No matching Lambda functions found in region:", region)
                continue

            print(f"The following Lambda functions in region {region} match the include patterns and do not match any exclude patterns. They will be considered for deletion:")
            for function_name in matching_functions:
                print(function_name)

            user_input = input("Do you want to delete these Lambda functions? (yes/no): ")
            if user_input.lower() == 'yes':
                for function_name in matching_functions:
                    print(f"Deleting Lambda function: {function_name}")
                    # Delete the Lambda function
                    delete_response = lambda_client.delete_function(FunctionName=function_name)
                    print(f"Deleted Lambda function: {function_name}, Status: {delete_response['ResponseMetadata']['HTTPStatusCode']}")
            else:
                print("Deletion cancelled by user.")
        except Exception as e:
            print(f"An error occurred in region {region}: {str(e)}")
def main():

    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*glue.*"]      # Regex patterns for lambda functions to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]  # Regex patterns for lambda functions to exclude from deletion
    # regions = ['us-east-1' 'us-east-2']


    include_patterns = []  
    exclude_patterns = []
    regions = ['us-east-1', 'us-east-2']
    delete_lambda_functions(include_patterns, exclude_patterns, regions)

if __name__ == '__main__':
    main()
    