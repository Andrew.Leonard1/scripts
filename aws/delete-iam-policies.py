import boto3
import re
from botocore.exceptions import ClientError

# Initialize a boto3 client
iam_client = boto3.client('iam')

def list_policies(include_patterns, exclude_patterns=None):
    """
    List IAM policies and filter them based on include and exclude regex patterns.
    :param include_patterns: List of regex patterns to include policies for deletion.
    :param exclude_patterns: List of regex patterns to exclude policies from deletion.
    :return: List of policy ARNs that match the criteria
    """
    if exclude_patterns is None:
        exclude_patterns = []
    filtered_policies = []
    paginator = iam_client.get_paginator('list_policies')
    
    # Only include customer managed policies, not AWS managed policies
    for page in paginator.paginate(Scope='Local'):
        for policy in page['Policies']:
            policy_name = policy['PolicyName']
            if any(re.search(pattern, policy_name) for pattern in include_patterns) and not any(re.search(pattern, policy_name) for pattern in exclude_patterns):
                filtered_policies.append(policy['Arn'])
    
    return filtered_policies

def detach_policy_from_entities(policy_arn):
    """
    Detach the specified policy from any users, groups, and roles it's attached to.
    :param policy_arn: ARN of the policy to detach
    """
    # Detach from users
    for user in iam_client.list_entities_for_policy(PolicyArn=policy_arn, EntityFilter='User')['PolicyUsers']:
        iam_client.detach_user_policy(UserName=user['UserName'], PolicyArn=policy_arn)
        print(f"Detached policy from user: {user['UserName']}")

    # Detach from groups
    for group in iam_client.list_entities_for_policy(PolicyArn=policy_arn, EntityFilter='Group')['PolicyGroups']:
        iam_client.detach_group_policy(GroupName=group['GroupName'], PolicyArn=policy_arn)
        print(f"Detached policy from group: {group['GroupName']}")

    # Detach from roles
    for role in iam_client.list_entities_for_policy(PolicyArn=policy_arn, EntityFilter='Role')['PolicyRoles']:
        iam_client.detach_role_policy(RoleName=role['RoleName'], PolicyArn=policy_arn)
        print(f"Detached policy from role: {role['RoleName']}")

def delete_iam_policy(policy_arn):
    """
    Deletes an IAM policy by its ARN after detaching it from all entities.
    :param policy_arn: ARN of the policy to delete
    """
    # First, detach the policy from any entities
    detach_policy_from_entities(policy_arn)
    
    # Then, delete the policy
    try:
        iam_client.delete_policy(PolicyArn=policy_arn)
        print(f"Policy {policy_arn} deleted successfully.\n")
    except ClientError as error:
        if error.response['Error']['Code'] == 'NoSuchEntity':
            print(f"Policy {policy_arn} does not exist.")
        else:
            print(f"Failed to delete policy {policy_arn}. Error: {error}")

def delete_iam_policies(include_patterns, exclude_patterns):
    """
    Deletes IAM policies based on include and exclude patterns
    :param include_patterns: List of regex patterns to include for IAM policy deletion.
    :param exclude_patterns: List of regex patterns for IAM policies to exclude from deletion.
    """
    matching_policies = list_policies(include_patterns, exclude_patterns)

    print(f"The following {len(matching_policies)} policies match the include patterns and do not match any exclude patterns. They will be considered for deletion:")
    for arn in matching_policies:
        print(arn)
    
    user_input = input("Do you want to delete these policies? (yes/no): ")
    if user_input.lower() == 'yes':
        for arn in matching_policies:
            delete_iam_policy(arn)
    else:
        print("Deletion cancelled by user.")


def main():

    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*glue.*"]      # Regex patterns for IAM policies to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]  # Regex patterns for IAM policies to exclude from deletion
    # regions = ['us-east-1' 'us-east-2']

    include_patterns = []
    exclude_patterns = []
    delete_iam_policies(include_patterns, exclude_patterns)
    
    

if __name__ == '__main__':
    main()

