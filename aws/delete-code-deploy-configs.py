import boto3
import re
from botocore.exceptions import ClientError

def delete_deployment_groups_and_configs(include_patterns, exclude_patterns=None, regions=None, delete_configs=False):
    """
    Lists and optionally deletes AWS CodeDeploy deployment groups and their attached deployment configurations across multiple regions 
    based on specified include patterns and excluding any that match patterns in exclude_patterns, after user confirmation.

    :param include_patterns: List of regex patterns to match deployment group names against for inclusion.
    :param exclude_patterns: List of regex patterns to exclude deployment groups and configs from deletion.
    :param regions: List of AWS regions to operate in.
    :param delete_configs: Boolean indicating whether to delete the deployment configurations attached to the deployment groups.
    """
    if exclude_patterns is None:
        exclude_patterns = []
    if regions is None:
        regions = ['us-east-1']  # Default region if none provided

    items_to_delete = []

    for region in regions:
        print(f"Processing region: {region}")
        codedeploy_client = boto3.client('codedeploy', region_name=region)

        try:
            response = codedeploy_client.list_applications()
            for application_name in response.get('applications', []):
                # List deployment groups for the application
                dg_response = codedeploy_client.list_deployment_groups(applicationName=application_name)
                deployment_groups = dg_response.get('deploymentGroups', [])

                for deployment_group_name in deployment_groups:
                    if any(re.search(pattern, deployment_group_name) for pattern in include_patterns) and not any(re.search(pattern, deployment_group_name) for pattern in exclude_patterns):
                        dg_info = codedeploy_client.get_deployment_group(applicationName=application_name, deploymentGroupName=deployment_group_name)
                        config_name = dg_info['deploymentGroupInfo']['deploymentConfigName']
                        items_to_delete.append((region, application_name, deployment_group_name, config_name))
        except ClientError as e:
            print(f"An error occurred in region {region}: {e}")

    # Display deletable items
    if items_to_delete:
        print("\nThe following deployment groups and their configurations are selected for deletion:")
        for item in items_to_delete:
            print(f"Region: {item[0]}, Application: {item[1]}, Deployment Group: {item[2]}, Config: {item[3]}")
        user_input = input("\nDo you want to proceed with the deletion? (yes/no): ")
        if user_input.lower() == 'yes':
            for region, application_name, deployment_group_name, config_name in items_to_delete:
                try:
                    codedeploy_client = boto3.client('codedeploy', region_name=region)
                    print(f"Deleting deployment group: {deployment_group_name} in application: {application_name}")
                    codedeploy_client.delete_deployment_group(applicationName=application_name, deploymentGroupName=deployment_group_name)
                    print(f"Deleted deployment group: {deployment_group_name}")

                    if delete_configs and not config_name.startswith('CodeDeployDefault.'):
                        print(f"Deleting deployment configuration: {config_name}")
                        codedeploy_client.delete_deployment_config(deploymentConfigName=config_name)
                        print(f"Deleted deployment configuration: {config_name}")
                except ClientError as e:
                    print(f"Failed to delete: {e}")
        else:
            print("Deletion cancelled by user.")
    else:
        print("No matching deployment groups or configurations found for deletion.")

# Example usage

def main():

    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*gluelog.*"]      # Regex patterns for code deploy configs to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]     # Regex patterns for code deploy configs to exclude from deletion

    include_patterns = []
    exclude_patterns = []
    regions = ['us-east-1', 'us-east-2']
    delete_deployment_groups_and_configs(include_patterns, exclude_patterns, regions, delete_configs=True)

if __name__ == '__main__':
    main()


