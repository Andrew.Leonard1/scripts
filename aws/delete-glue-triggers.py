import boto3
import re

# Initialize a boto3 client
glue_client = boto3.client('glue')

def delete_glue_triggers(include_patterns, exclude_patterns=None):
    """
    Deletes AWS Glue triggers based on specified include patterns and excluding any that match patterns in exclude_patterns,
    after user confirmation, with pagination handled.

    :param include_patterns: List of string patterns to match trigger names against for inclusion.
    :param exclude_patterns: List of string patterns to exclude triggers from deletion.
    """
    if exclude_patterns is None:
        exclude_patterns = []

    try:
        matching_triggers = []
        next_token = None

        # Handle pagination
        while True:
            if next_token:
                response = glue_client.get_triggers(NextToken=next_token)
            else:
                response = glue_client.get_triggers()

            if 'Triggers' in response:
                for trigger in response['Triggers']:
                    trigger_name = trigger['Name']
                    if any(re.search(include_pattern, trigger_name) for include_pattern in include_patterns) and not any(re.search(exclude_pattern, trigger_name) for exclude_pattern in exclude_patterns):
                        matching_triggers.append(trigger_name)

            next_token = response.get('NextToken')
            if not next_token:
                break

        if not matching_triggers:
            print("No matching triggers found.")
            return

        print("The following triggers match the include patterns and do not match any exclude patterns. They will be considered for deletion:")
        for trigger_name in matching_triggers:
            print(trigger_name)

        # Ask for user confirmation
        user_input = input("Do you want to delete these triggers? (yes/no): ")
        if user_input.lower() == 'yes':
            for trigger_name in matching_triggers:
                print(f"Deleting trigger: {trigger_name}")
                delete_response = glue_client.delete_trigger(Name=trigger_name)
                print(f"Deleted trigger: {trigger_name}, Status: {delete_response['ResponseMetadata']['HTTPStatusCode']}")
        else:
            print("Deletion cancelled by user.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")


def main():
    
    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*gluelog.*"]      # Regex patterns for Glue triggers to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]     # Regex patterns for Glue triggers to exclude from deletion
    
    include_patterns = []
    exclude_patterns = []
    delete_glue_triggers(include_patterns, exclude_patterns)

if __name__ == "__main__":
    main()

