import boto3
import re
from botocore.exceptions import ClientError

iam_client = boto3.client('iam')

def list_roles(include_patterns, exclude_patterns=None):
    """
    List IAM roles and filter them based on include and exclude regex patterns.
    :param include_patterns: List of regex patterns to include roles for deletion.
    :param exclude_patterns: List of regex patterns to exclude roles from deletion.
    :return: List of role names that match the criteria
    """
    if exclude_patterns is None:
        exclude_patterns = []
    filtered_roles = []
    paginator = iam_client.get_paginator('list_roles')
    
    for page in paginator.paginate():
        for role in page['Roles']:
            role_name = role['RoleName']
            if any(re.search(pattern, role_name) for pattern in include_patterns) and not any(re.search(pattern, role_name) for pattern in exclude_patterns):
                filtered_roles.append(role_name)
    
    return filtered_roles

def detach_all_policies_from_role(role_name):
    """
    Detaches all managed policies from the specified IAM role.
    :param role_name: Name of the IAM role
    """
    attached_policies = iam_client.list_attached_role_policies(RoleName=role_name)['AttachedPolicies']
    for policy in attached_policies:
        iam_client.detach_role_policy(RoleName=role_name, PolicyArn=policy['PolicyArn'])
        print(f"Detached policy {policy['PolicyArn']} from role {role_name}")

def delete_role_instance_profiles(role_name):
    """
    Deletes all instance profiles associated with the specified IAM role.
    :param role_name: Name of the IAM role
    """
    instance_profiles = iam_client.list_instance_profiles_for_role(RoleName=role_name)['InstanceProfiles']
    for profile in instance_profiles:
        iam_client.remove_role_from_instance_profile(InstanceProfileName=profile['InstanceProfileName'], RoleName=role_name)
        print(f"Removed role {role_name} from instance profile {profile['InstanceProfileName']}")
        iam_client.delete_instance_profile(InstanceProfileName=profile['InstanceProfileName'])
        print(f"Deleted instance profile {profile['InstanceProfileName']}")

def delete_iam_role(role_name):
    """
    Deletes an IAM role after detaching all policies and deleting associated instance profiles.
    :param role_name: Name of the IAM role to delete
    """
    detach_all_policies_from_role(role_name)
    delete_role_instance_profiles(role_name)
    
    try:
        iam_client.delete_role(RoleName=role_name)
        print(f"Role {role_name} deleted successfully.\n")
    except ClientError as error:
        print(f"Failed to delete role {role_name}. Error: {error}")

def delete_roles(include_patterns, exclude_patterns):
    """
    Deletes IAM roles based on include and exclude patterns
    :param include_patterns: List of regex patterns to include roles for deletion.
    :param exclude_patterns: List of regex patterns to exclude roles from deletion.
    """
    matching_roles = list_roles(include_patterns, exclude_patterns)
    print(f"The following {len(matching_roles)} roles match the include patterns and do not match any exclude patterns. They will be considered for deletion:")
    for role in matching_roles:
        print(role)

    user_input = input("Do you want to delete these roles? (yes/no): ")
    if user_input.lower() == 'yes':
        for role_name in matching_roles:
            delete_iam_role(role_name)
    else:
        print("Deletion cancelled by user.")

def main():

    ###############################################
    # Update these values based on your use case
    ###############################################
    # Example
    # include_patterns = ["ccapi-customer-api-104953-dev.*gluelog.*"]      # Regex patterns for IAM roles to delete
    # exclude_patterns = ["ccapi-customer-api-104953-dev-f-api-12345"]     # Regex patterns for IAM roles to exclude from deletion

    
    include_patterns = []
    exclude_patterns = []
    delete_roles(include_patterns, exclude_patterns)
    
if __name__ == '__main__':
    main()

